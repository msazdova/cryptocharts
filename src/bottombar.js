var BottomBar = {
  controller: function() {

  },
  view: function(ctrl) {
    return m('div', {class: 'col-md-12'}, [
        m('div', {class: 'navbar navbar-default navbar-fixed-bottom'}, [
            m('div', {class: 'container'}, [
              m('div', { class: 'navbar-footer' }, [
                m('a', { class: 'navbar-brand', href: '#' }, 'BOTTOM BAR')
            ])
        ])
    ])
    ]);
  }
};
